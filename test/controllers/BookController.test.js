'use strict';

const request = require('supertest');

const app = require('../../src/app');
const { User, Book } = require('../../src/models')

let ownerBook;

beforeEach(async () => {
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })
    ownerBook = await User.create({ name: 'Marlon Reis', email: 'marlon-reis@hotmail.com' });
});

afterAll(async () => {
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })
});

describe('Request POST to create a new book', () => {
    it('Shoud be create new book register and it`s return the data in response body', async (done) => {
        await request(app).post("/loanbook/books")
            .send({ title: 'SCRUM', page: 238, ownerId: ownerBook.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                const { id, title, page, ownerId, createdAt } = res.body;
                expect(id).toBeDefined();
                expect(title).toBe('SCRUM');
                expect(page).toBe(238);
                expect(ownerId).toBe(ownerBook.id);
                expect(createdAt).toBeDefined();
            });
        done();
    });

    it('Shoud be throw error when title is empty', async (done) => {
        await request(app).post("/loanbook/books")
            .send({ title: '', page: 238, ownerId: ownerBook.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors)
                    .toContainEqual(expect.objectContaining({
                        value: "",
                        msg: "Invalid title!",
                        param: "title",
                        location: "body"
                    }))
                done()
            });
        done();
    });

    it('Shoud be throw error when page is 0', async (done) => {
        await request(app).post("/loanbook/books")
            .send({ title: 'The lord of the rings', page: 0, ownerId: ownerBook.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors)
                    .toContainEqual(expect.objectContaining({
                        location: "body",
                        msg: "page must be greater than 0",
                        param: "page",
                        value: 0
                    }))
                done()
            });
        done();
    });

    it('Shoud be throw not found when not found user', async (done) => {
        await request(app).post("/loanbook/books")
            .send({ title: 'The lord of the rings', page: 1200, ownerId: 48458254 })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(404).expect(function (res) {
                expect(res.body.msg).toBe("User(owner) with id is equals '48458254', not found!")
            });
        done();
    });

})