
'use strict';

const request = require('supertest');

const app = require('../../src/app');
const { User, Book, BookLoan } = require('../../src/models')

let ownerBook;
let userWhoIsReceivingTheBook
let bookToBeBorrowed;

beforeEach(async () => {
    await BookLoan.destroy({ where: {} })
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })

    userWhoIsReceivingTheBook = await User.create({ name: 'João Luiz', email: 'joao@hotmail.com' });
    ownerBook = await User.create({ name: 'Marlon Reis', email: 'marlon-reis@hotmail.com' });
    bookToBeBorrowed = await Book.create({ title: '+ esperto que o diabo', page: 136, ownerId: ownerBook.id })

});

afterAll(async () => {
    await BookLoan.destroy({ where: {} })
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })
});


describe('Request POST to loan book', () => {
    it('Shoud be loan book and return http status 200 and return loan details', async (done) => {
        await request(app).post("/loanbook/loans")
            .send({
                fromUser: ownerBook.id,
                toUser: userWhoIsReceivingTheBook.id,
                book: bookToBeBorrowed.id
            })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                const { id, lentAt, fromUser, toUser, book } = res.body;
                expect(id).toBeDefined();
                expect(lentAt).toBeDefined();
                expect(fromUser).toBe(ownerBook.id);
                expect(toUser).toBe(userWhoIsReceivingTheBook.id);
                expect(book).toBe(bookToBeBorrowed.id);
            });
        done();
    });

    it('Shoud be return http status 422 when trying to borrow the book already borrowed', async (done) => {
        await BookLoan.create({ fromUser: ownerBook.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })
        await request(app).post("/loanbook/loans")
            .send({ fromUser: ownerBook.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.msg).toBe("Sorry, but this book is not available!");
            });
        done();
    });

    it('Shoud be return http status 422 when the book to be borrowed does not belong to the user', async (done) => {
        const otherUser = await User.create({ name: 'Luiz Miguel', email: 'luizmiguel@hotmail.com' });
        await request(app).post("/loanbook/loans")
            .send({ fromUser: otherUser.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.msg).toBe(`This book does not belong to the user with id is '${otherUser.id}'`);
            });
        done();
    });

    it('Shoud be return http status 422 when the book owner tries to lend a book to himself', async (done) => {
        await request(app).post("/loanbook/loans")
            .send({ fromUser: ownerBook.id, toUser: ownerBook.id, book: bookToBeBorrowed.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.msg).toBe('Users must be different');
            });
        done();
    });
})


describe('Request  get find all lend book', () => {
    it('Shoud be find all lend book and return http status code 200', async (done) => {
        await BookLoan.create({ fromUser: ownerBook.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })

        await request(app).get("/loanbook/loans")
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                expect(res.body).toContainEqual(expect.objectContaining({
                    id: expect.any(Number),
                    lentAt: expect.anything(),
                    borrowed_book: expect.anything(),
                    from_user: expect.anything(),
                    to_user: expect.anything()
                }))
            });

        done();
    });

    it('Shoud be return http status code 200 and empty array when not found loan book', async (done) => {
        await request(app).get("/loanbook/loans")
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200, [])
        done();
    });
})


describe('Request  get find lend book by id', () => {
    it('Shoud be find lend book by id and return http status code 200', async (done) => {
        const bookLoan = await BookLoan.create({ fromUser: ownerBook.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })
        await request(app).get(`/loanbook/loans/${bookLoan.id}`)
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                expect(res.body).toEqual(expect.objectContaining({
                    id: expect.any(Number),
                    lentAt: expect.anything(),
                    borrowed_book: expect.anything(),
                    from_user: expect.anything(),
                    to_user: expect.anything()
                }))
            });

        done();
    });

    it('Shoud be return http status code 404  ', async (done) => {
        await request(app).get(`/loanbook/loans/404`)
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(404).expect(function (res) {
                expect(res.body.msg).toEqual("Cannot found loan book by id='404'")
            });
        done();
    });
})


describe('Request PUT to return the book', () => {
    it('Shoud be return the book and http status 200', async (done) => {
        await BookLoan.create({ fromUser: ownerBook.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })
        await request(app).put("/loanbook/loans")
            .send({
                fromUser: ownerBook.id,
                book: bookToBeBorrowed.id
            })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                expect(res.body).toEqual(expect.objectContaining({
                    id: expect.any(Number),
                    lentAt: expect.anything(),
                    returnedAt: expect.anything(),
                    borrowed_book: expect.anything(),
                    from_user: expect.anything(),
                    to_user: expect.anything()
                }))
            });
        done();
    });


    it('Shoud be http status 422 and error message when trying to return a book twice', async (done) => {
        await BookLoan.create({ fromUser: ownerBook.id, toUser: userWhoIsReceivingTheBook.id, book: bookToBeBorrowed.id })

        await request(app).put("/loanbook/loans").send({ fromUser: ownerBook.id, book: bookToBeBorrowed.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/);

        await request(app).put("/loanbook/loans").send({ fromUser: ownerBook.id, book: bookToBeBorrowed.id })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.msg).toEqual("Sorry, but this book has already been returned!")
            });
        done();
    });


})