const request = require('supertest');

const app = require('../../src/app');
const { User } = require('../../src/models')

beforeEach(async () => {
    await User.destroy({ where: {} })
});

afterAll(async () => {
    await User.destroy({ where: {} })
});


describe('Request POST to create a new user', () => {
    it('Shoud be create new register and it`s return the data in response body', async (done) => {
        await request(app).post("/loanbook/users")
            .send({ name: 'Luiz Miguel', email: 'luiz@miguel.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                const { id, name, email, createdAt } = res.body;
                expect(id).toBeDefined();
                expect(name).toBe('Luiz Miguel');
                expect(email).toBe('luiz@miguel.com.br');
                expect(createdAt).toBeDefined();
                done()
            });
    })
})

describe('Validate the data before trying to create register', () => {

    it('Shoud be throw error 422 when email duplicate', async (done) => {
        await User.create({ name: 'Luiz Miguel', email: 'luiz@miguel.com.br' });
        await request(app).post("/loanbook/users")
            .send({ name: 'Luiz Miguel', email: 'luiz@miguel.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                const { param, value, msg } = res.body;
                expect(param).toBe('email');
                expect(value).toBe('luiz@miguel.com.br');
                expect(msg).toBe('This email is already being used in another account!');
            });
        done();
    })

    it('Shoud be throw error 422 when name is empty', async (done) => {
        await request(app).post("/loanbook/users")
            .send({ name: '', email: 'luiz@miguel.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors)
                    .toContainEqual(expect.objectContaining({
                        value: '',
                        msg: 'Invalid name!',
                        param: 'name',
                        location: 'body'
                    }))
                done()
            });
    })

    it('Shoud be throw error 422 when name is not defined', async (done) => {
        await request(app).post("/loanbook/users")
            .send({ email: 'luiz@miguel.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors)
                    .toContainEqual(expect.objectContaining(
                        {
                            msg: 'Invalid name!',
                            param: 'name',
                            location: 'body'
                        }))
                done()
            });
    })

    it('Shoud be throw error 422 when email is invalid', async (done) => {
        await request(app).post("/loanbook/users")
            .send({ name: 'Marlon Reis', email: 'marlon-reis@hotmail' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors).toContainEqual(expect.objectContaining(
                    {
                        location: "body",
                        msg: "invalid email!",
                        param: "email",
                        value: "marlon-reis@hotmail"
                    }
                ))
                done()
            });
    })

    it('Shoud be throw error 422 when email is not defined', async (done) => {
        await request(app).post("/loanbook/users")
            .send({ name: 'Marlon Reis' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors).toContainEqual(expect.objectContaining(
                    {
                        location: "body",
                        msg: "invalid email!",
                        param: "email"
                    }
                ))
                done()
            });
    })
})

describe('Find user register by id', () => {
    it('Shoud be find user by id and return data in body whe use GET method', async (done) => {
        const user = await User.create({ name: 'Luiz Miguel', email: 'luiz@miguel.com.br' });
        await request(app).get(`/loanbook/users/${user.id}`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                const { id, name, email, createdAt } = res.body;
                expect(id).toBeDefined();
                expect(name).toBe('Luiz Miguel');
                expect(email).toBe('luiz@miguel.com.br');
                expect(createdAt).toBeDefined();
            });
        done()
    })

    it('Shoud be return status 404 when not found user register by id', async (done) => {
        await request(app).get("/loanbook/users/2020")
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404).expect(function (res) {
                const { msg } = res.body
                expect(msg).toBe("Cannot found user by id='2020'");
            });
        done()
    })
})

describe('Request PUT to update user', () => {
    it('Shoud be update register and it`s return the data in response body', async (done) => {
        const user = await User.create({ name: 'Luiz Miguel', email: 'luiz@miguel.com.br' });
        await request(app).put("/loanbook/users")
            .send({ id: user.id, name: 'João Luiz', email: 'joao@luiz.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                const { id, name, email, createdAt } = res.body;
                expect(id).toBeDefined();
                expect(name).toBe('João Luiz');
                expect(email).toBe('joao@luiz.com.br');
                expect(createdAt).toBeDefined();
            });
        done();
    });
})


describe('Validate the data before trying to update register', () => {
    it('Shoud be throw error 422 when name is empty', async (done) => {
        await request(app).put("/loanbook/users")
            .send({ name: '', email: 'luiz@miguel.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors)
                    .toContainEqual(expect.objectContaining({
                        value: '',
                        msg: 'Invalid name!',
                        param: 'name',
                        location: 'body'
                    }))
                done()
            });
    })

    it('Shoud be throw error 422 when name is not defined', async (done) => {
        await request(app).put("/loanbook/users")
            .send({ email: 'luiz@miguel.com.br' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors)
                    .toContainEqual(expect.objectContaining(
                        {
                            msg: 'Invalid name!',
                            param: 'name',
                            location: 'body'
                        }))
                done()
            });
    })

    it('Shoud be throw error 422 when email is invalid', async (done) => {
        await request(app).put("/loanbook/users")
            .send({ name: 'Marlon Reis', email: 'marlon-reis@hotmail' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors).toContainEqual(expect.objectContaining(
                    {
                        location: "body",
                        msg: "invalid email!",
                        param: "email",
                        value: "marlon-reis@hotmail"
                    }
                ))
                done()
            });
    })

    it('Shoud be throw error 422 when email is not defined', async (done) => {
        await request(app).put("/loanbook/users")
            .send({ name: 'Marlon Reis' })
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(422).expect(function (res) {
                expect(res.body.errors).toContainEqual(expect.objectContaining(
                    {
                        location: "body",
                        msg: "invalid email!",
                        param: "email"
                    }
                ))
                done()
            });
    })
})
//findUserWithBookCollerctionById
describe('Find user with book collections by id', () => {
    it('Shoud be find user with book collections by id and return collections empty ', async (done) => {
        const user = await User.create({ name: 'Luiz Miguel', email: 'luiz@miguel.com.br' });
        await request(app).get(`/loanbook/users/${user.id}/collerctions`)
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(200).expect(function (res) {
                const { id, name, email, collections } = res.body;
                expect(id).toBeDefined();
                expect(name).toBe('Luiz Miguel');
                expect(email).toBe('luiz@miguel.com.br');
                expect(collections).toEqual([])
            });
        done();
    });

    it('Shoud be throw 404 error when not find user with book collections by id', async (done) => {
        await request(app).get('/loanbook/users/404/collerctions')
            .set('Accept', 'application/json').expect('Content-Type', /json/)
            .expect(404).expect(function (res) {
                expect(res.body.msg).toBe("Cannot found user by id='404'");
            });
        done();
    });

});




