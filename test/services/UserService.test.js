'use strict';

const { User } = require('../../src/models')
const UserService = require('../../src/services/UserService');
const service = new UserService();

beforeEach(async () => {
    await User.destroy({ where: {} })
});

afterAll(async () => {
    await User.destroy({ where: {} })
});

describe('Create new user register', () => {
    it('Shoud be create new user register and it`s return the data', async () => {
        const user = await service.create('João Luiz', 'joao@luiz.com.br')
        expect(user.id).toBeDefined();
        expect(user.name).toBe('João Luiz')
        expect(user.email).toBe('joao@luiz.com.br');
    });

    it('Shoud be throw error when tried create duplicate register', async () => {
        try {
            await service.create('João Luiz', 'joao@luiz.com.br')
            await service.create('João Luiz', 'joao@luiz.com.br')
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeUniqueConstraintError')
            expect(errors).toContainEqual(expect.objectContaining({
                path: 'email',
                value: 'joao@luiz.com.br',
            }))
        }
    });

    it('Shoud be throw error when email is invalid', async () => {
        try {
            await service.create('João Luiz', 'joaoluiz.com.br')
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError')
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'Invalid e-mail!',
                type: 'Validation error',
                path: 'email',
                value: 'joaoluiz.com.br',
                origin: 'FUNCTION',
                validatorKey: 'isEmail',
                validatorName: 'isEmail'
            }))
        }
    });

    it('Shoud be throw error when name is null', async () => {
        try {
            await service.create(undefined, 'joao@luiz.com.br')
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError')
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'User.name cannot be null',
                type: 'notNull Violation',
                path: 'name',
                origin: 'CORE',
                validatorKey: 'is_null'
            }))
        }
    });
})


describe('Update user register', () => {
    it('Shoud be update the user register', async () => {
        const user = await service.create('João Luiz', 'joao@luiz.com.br');
        const response = await service.update(user.id, 'João Miguel', 'joao@miguel.com.br');
        expect(response.id).toBeDefined();
        expect(response.name).toBe('João Miguel')
        expect(response.email).toBe('joao@miguel.com.br');
    });

    it('Shoud be update the user register', async () => {
        try {
            const user = await service.create('João Luiz', 'joao@luiz.com.br');
            await service.update(user.id, 'João Miguel', 'joaomiguel.com.br');
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError')
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'Invalid e-mail!',
                type: 'Validation error',
                path: 'email',
                value: 'joaomiguel.com.br',
                origin: 'FUNCTION',
                validatorKey: 'isEmail',
                validatorName: 'isEmail'
            }))
        }
    });
});


describe('Find user by id', () => {
    it('Shoud be found user by id when user exist', async () => {
        const user = await service.create('João Luiz', 'joao@luiz.com.br');
        const response = await service.findUserById(user.id);
        expect(response.id).toBeDefined();
        expect(response.name).toBe('João Luiz')
        expect(response.email).toBe('joao@luiz.com.br');
    });


    it('Shoud be return null when not found register', async () => {
        const response = await service.findUserById(2020);
        expect(response).toBeNull();
    });
});



describe('Find user by email', () => {
    it('Shoud be found user by email when user exist', async () => {
        const user = await service.create('João Luiz', 'joao@luiz.com.br');
        const response = await UserService.findUserByEmail(user.email);
        expect(response.id).toBeDefined();
        expect(response.name).toBe('João Luiz')
        expect(response.email).toBe('joao@luiz.com.br');
    });


    it('Shoud be return null when not found register by email', async () => {
        const response = await UserService.findUserByEmail('any@email.com.br');
        expect(response).toBeNull();
    });
});


describe('find user with book collerction by id', () => {
    it('Shoud be found user with book collerction by id', async () => {
        const user = await service.create('João Luiz', 'joao@luiz.com.br');
        const response = await service.findUserWithBookCollerctionById(user.id);

        expect(response.id).toBeDefined();
        expect(response.name).toBe('João Luiz')
        expect(response.email).toBe('joao@luiz.com.br');
        expect(response.collections).toEqual([])
    });


    it('Shoud be return null when not found register by id', async () => {
        const response = await service.findUserWithBookCollerctionById(404);
        expect(response).toBeNull();
    });
});