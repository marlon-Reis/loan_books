'use strict';

const { User, Book } = require('../../src/models')

const BookService = require('../../src/services/BookService');
const UserService = require('../../src/services/UserService');

const bookService = new BookService();
const userService = new UserService();

let user = undefined;

beforeEach(async () => {
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })

    user = await userService.create('Marlon Reis', 'marlon-reis@hotmail.com');
});

afterAll(async () => {
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })
});


describe('Create new book register', () => {

    it('Should be create register', async () => {
        const book = await bookService.create('Sherlock Homes', 1200, user.id);

        expect(book.id).toBeDefined();
        expect(book.title).toBe('Sherlock Homes');
        expect(book.page).toBe(1200);
        expect(book.ownerId).toBe(user.id);
    });
})

describe('Validate book register', () => {
    it('Should be throw error when title is undefined', async () => {
        try {
            await bookService.create(undefined, 1200, user.id);
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError');
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'Book.title cannot be null',
                type: 'notNull Violation',
                path: 'title',
                origin: 'CORE',
                validatorKey: 'is_null'
            }))
        }
    });

    it('Should be throw error when title is empty', async () => {
        try {
            await bookService.create('', 300, user.id);
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError');
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'title cannot is empty',
                origin: 'FUNCTION',
                path: 'title',
                type: 'Validation error',
                validatorKey: 'notEmpty',
                validatorName: 'notEmpty',
                value: ''
            }))
        }
    });


    it('Should be throw error when page is undefined', async () => {
        try {
            await bookService.create('Redes Neurais Artificiais', undefined, user.id);
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError');
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'Book.page cannot be null',
                origin: 'CORE',
                path: 'page',
                type: 'notNull Violation',
                validatorKey: 'is_null'
            }))
        }
    });

    it('Should be throw error when page <= 0', async () => {
        try {
            await bookService.create('Redes Neurais Artificiais', 0, user.id);
        } catch ({ name, errors }) {
            expect(name).toBe('SequelizeValidationError');
            expect(errors).toContainEqual(expect.objectContaining({
                message: 'pages must be greater than 0',
                origin: 'FUNCTION',
                path: 'page',
                type: 'Validation error',
                validatorKey: 'min',
                validatorName: 'min'
            }))
        }
    });
});

describe('Find book by title and owner id ', () => {
    it('Should be find book by title ignore case and owner id', async () => {
        const book = await bookService.create('The Hobbit', 1200, user.id);
        bookService.findBookByTitleIgnoreCaseAndOwnerId('THE HOBBIT', user.id).
            then((bookFound, error) => {
                expect(bookFound.id).toBeDefined();
                expect(bookFound.title).toBe(book.title);
                expect(bookFound.ownerId).toBe(book.ownerId);
                expect(bookFound.ownerId).toBe(user.id);
            })
    });
})

describe('Find book by id ', () => {
    it('Should be find book by id', async () => {
        const book = await bookService.create('The Hobbit', 1200, user.id);
        bookService.findBookById(book.id).
            then((bookFound, error) => {
                expect(bookFound.id).toBeDefined();
                expect(bookFound.title).toBe(book.title);
                expect(bookFound.ownerId).toBe(book.ownerId);
                expect(bookFound.ownerId).toBe(user.id);
            })
    });
})