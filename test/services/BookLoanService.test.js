'use strict';

const { User, Book, BookLoan } = require('../../src/models')

const BookLoanService = require('../../src/services/BookLoanService');
const service = new BookLoanService();

let fromUser = undefined;
let toUser = undefined;
let book = undefined;

beforeEach(async () => {
    await BookLoan.destroy({ where: {} })
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })

    fromUser = await User.create({ name: 'Marlon Reis', email: 'marlon-reis@hotmail.com' });
    toUser = await User.create({ name: 'João Luiz', email: 'joao_luiz@hotmail.com' });
    book = await Book.create({
        title: '+ esperto que o diabo', page: 136, ownerId: fromUser.id
    })


});

afterAll(async () => {
    await BookLoan.destroy({ where: {} })
    await Book.destroy({ where: {} })
    await User.destroy({ where: {} })
});


describe('lend Book', () => {
    it('Shoud be loan book and return data', async () => {
        const response = await service.lendBook(fromUser.id, toUser.id, book.id);
        expect(response.id).toBeDefined();
        expect(response.fromUser).toBe(fromUser.id)
        expect(response.toUser).toBe(toUser.id);
        expect(response.book).toBe(book.id);
    });
});

//findAllLendBook
describe('Find all borrowed books', () => {
    it('Shoud be find all lend books', async () => {
        await service.lendBook(fromUser.id, toUser.id, book.id);
        const response = await service.findAllLendBook()
        expect(response).toContainEqual(expect.objectContaining({
            "id": expect.any(Number),
            "lentAt": expect.any(Date),
            "borrowed_book": expect.any(Object),
            "from_user": expect.any(Object),
            "to_user": expect.any(Object)
        }))
    });

    it('Shoud be return empty array when not found borrowed books', async () => {
        const response = await service.findAllLendBook()
        expect(response).toEqual([])
    });
})


describe('This book is on loan', () => {
    it('Shoud be return the book loan if it is borrowed', async () => {
        await service.lendBook(fromUser.id, toUser.id, book.id);
        const response = await service.thisBookIsOnLoan(book.id)

        expect(response.id).toBeDefined();
        expect(response.lentAt).toBeDefined();
        expect(response.returnedAt).toBeNull();
        expect(response.fromUser).toBeDefined();
        expect(response.toUser).toBeDefined();
        expect(response.book).toBe(book.id);

    });

    it('Shoud be return null when book is not on a loan', async () => {
        const response = await service.thisBookIsOnLoan(404)
        expect(response).toBeNull()
    });

})


describe('find book loand by id', () => {
    it('Shoud be find book by id if exist', async () => {
        const loanBook = await service.lendBook(fromUser.id, toUser.id, book.id);
        const response = await service.findBookLoanById(loanBook.id)

        expect(response.id).toBeDefined();
        expect(response.lentAt).toBeDefined();
        expect(response.borrowed_book).toBeDefined();
        expect(response.from_user).toBeDefined();
        expect(response.to_user).toBeDefined();
    });

    it('Shoud be return null when not found', async () => {
        const response = await service.findBookLoanById(404)
        expect(response).toBeNull()
    });

})

describe('find book loand by id', () => {
    it('Shoud be find book by id if exist', async () => {
        const loanBook = await service.lendBook(fromUser.id, toUser.id, book.id);
        const response = await service.findBookLoanById(loanBook.id)

        expect(response.id).toBeDefined();
        expect(response.lentAt).toBeDefined();
        expect(response.borrowed_book).toBeDefined();
        expect(response.from_user).toBeDefined();
        expect(response.to_user).toBeDefined();
    });

    it('Shoud be return null when not found', async () => {
        const response = await service.findBookLoanById(404)
        expect(response).toBeNull()
    });

})

describe('return the Book', () => {
    it('Shoud be return the loand book', async () => {
        await service.lendBook(fromUser.id, toUser.id, book.id);
        const response = await service.returnTheBook(fromUser.id, book.id)

        expect(response.id).toBeDefined();
        expect(response.lentAt).toBeDefined();
        expect(response.borrowed_book).toBeDefined();
        expect(response.from_user).toBeDefined();
        expect(response.to_user).toBeDefined();
    });

    it('Shoud be throw error when not found borrowed book ', async () => {
        try {
            await service.returnTheBook(fromUser.id, book.id)
        } catch (err) {
            expect(err.message).toBe("Register not updated");
        }
    });

})




