const { check } = require('express-validator');

const BookService = require('../services/BookService')
const BookLoanService = require('../services/BookLoanService')

const bookService = new BookService();
const bookLoanService = new BookLoanService();


exports.checkBodyAttribute = [
    check('fromUser').matches(/[1-9]{1,5}/).withMessage("Invalid fromUser!"),
    check("toUser").matches(/[1-9]{1,5}/).withMessage("Invalid toUser!"),
    check("book").matches(/[1-9]{1,5}/).withMessage("Invalid book!"),
];

exports.checkReturnTheBookBodyAttribute = [
    check('fromUser').matches(/[1-9]{1,5}/).withMessage("Invalid fromUser!"),
    check("book").matches(/[1-9]{1,5}/).withMessage("Invalid book!"),
];

exports.checkUsersAreDifferent = (req, res, next) => {
    const { toUser, fromUser } = req.body;
    if (toUser === fromUser) {
        res.status(422).json({
            msg: 'Users must be different'
        });
    } else {
        next();
    }
}

exports.checksIfTheUserHasThisBook = async (req, res, next) => {
    const { book, fromUser } = req.body;
    bookService.findBookByIdAndOwnerId(book, fromUser).then((data, err) => {
        responseError(err, res);
        if (data) {
            next();
        } else {
            res.status(422).json({
                msg: `This book does not belong to the user with id is '${fromUser}'`
            });
        }
    });
};

exports.checksIfThisBookIsOnLoan = async (req, res, next) => {
    bookLoanService.thisBookIsOnLoan(req.body.book).then((data, err) => {
        responseError(err, res);
        if (data) {
            res.status(422).json({ msg: 'Sorry, but this book is not available!' });
        } else {
            next()
        }
    });
};

exports.checksIfCanReturnBook = async (req, res, next) => {
    bookLoanService.thisBookIsOnLoan(req.body.book).then((data, err) => {
        responseError(err, res);
        if (data) {
            next();
        } else {
            res.status(422).json({ msg: 'Sorry, but this book has already been returned!' });
        }
    });
};





function responseError(err, res) {
    if (err) {
        res.status(500).json({ message: err.message });
    }
}

