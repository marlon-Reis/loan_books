'use strict';

const { check } = require('express-validator');
const service = require('../services/UserService')

exports.checkBodyAttribute = [
    check('name').notEmpty().withMessage("Invalid name!"),
    check("email").isEmail().withMessage("invalid email!")
];


exports.emailIsUnique = async (req, res, next) => {
    const { email } = req.body;
    service.findUserByEmail(email).then((data, err) => {
        if (err) {
            res.status(500).json({ message: err.message });
        }
        if (data) {
            res.status(422).json({
                param: 'email',
                value: email,
                msg: `This email is already being used in another account!`
            });
        } else {
            next();
        }
    });
};
