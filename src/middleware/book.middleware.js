const { check } = require('express-validator');

const BookService = require('../services/BookService')
const UserService = require('../services/UserService')

const bookService = new BookService();
const userService = new UserService()

exports.checkBodyAttribute = [
    check('title').notEmpty().withMessage("Invalid title!"),
    check("page").matches(/[1-9]{1,5}/).withMessage("page must be greater than 0"),
    check("ownerId").matches(/[1-9]{1,5}/).withMessage("Invalid ownerId!")
];


exports.checksIfTheUserHasThisBook = async (req, res, next) => {
    const { title, ownerId } = req.body;
    bookService.findBookByTitleIgnoreCaseAndOwnerId(title, ownerId).then((data, err) => {
        if (err) {
            res.status(500).json({ message: err.message });
        }
        if (data) {
            res.status(422).json({
                msg: `I'm sorry, you already registered this book!`
            });
        } else {
            next();
        }
    });
};

exports.checksIfUserExist = async (req, res, next) => {
    const { ownerId } = req.body;
    const user = await userService.findUserById(ownerId);
    if (user) {
        next();
    } else {
        res.status(404).json({
            msg: `User(owner) with id is equals '${ownerId}', not found!`
        });
    }
};
