'use strict';
const {
  Model
} = require('sequelize');

const DateWithTimezone = require('../util/DateWithTimezone');

module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    static associate(models) {
      Book.hasMany(models.BookLoan, {
        foreignKey: 'book',
        as: 'borrowed_book'
      })

      Book.belongsTo(models.User, {
        foreignKey: 'ownerId',
        as: 'collections'
      })
    }
  };
  Book.init({
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'title cannot is empty'
        }
      }
    },
    page: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: {
          args: [1],
          msg: 'pages must be greater than 0'
        }
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: new DateWithTimezone()
    }
  }, {
    sequelize,
    modelName: 'Book',
    tableName: 'Book',
    updatedAt: false
  });

  return Book;
};