'use strict';
const { Model } = require('sequelize');

const DateWithTimezone = require('../util/DateWithTimezone');


module.exports = (sequelize, DataTypes) => {
  class BookLoan extends Model {
    static associate(models) {
      BookLoan.belongsTo(models.Book, {
        foreignKey: 'book',
        as: 'borrowed_book'
      })

      BookLoan.belongsTo(models.User, {
        foreignKey: 'fromUser',
        as: 'from_user'
      })

      BookLoan.belongsTo(models.User, {
        foreignKey: 'toUser',
        as: 'to_user'
      })

    }
  };
  BookLoan.init({
    lentAt: {
      type: DataTypes.DATE,
      defaultValue: new DateWithTimezone()
    },
    returnedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'BookLoan',
    tableName: 'BookLoan',
    createdAt: false,
    updatedAt: false
  });




  return BookLoan;
};