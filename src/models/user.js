'use strict';

const { Model } = require('sequelize');
const DateWithTimezone = require('../util/DateWithTimezone');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      User.hasMany(models.Book, {
        foreignKey: 'ownerId',
        as: 'collections'
      })

      User.hasMany(models.BookLoan, {
        foreignKey: 'fromUser',
        as: 'from_user'
      })

      User.hasMany(models.BookLoan, {
        foreignKey: 'toUser',
        as: 'to_user'
      })

    }

  };

  User.init({
    name: {
      type: DataTypes.STRING(45),
      allowNull: false,
      notEmpty: {
        msg: "name cannot is empty!",
      }
    },
    email: {
      type: DataTypes.STRING(65),
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: "Invalid e-mail!",
        },
        notEmpty: {
          msg: "e-mail cannot is empty!",
        },
        max: {
          arg: [65],
          msg: 'Invalid e-mail! Contains more than 65 characters.'
        },
        min: {
          arg: [6],
          msg: "Invalid e-mail! It's very short."
        }
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: new DateWithTimezone()
    },
  },
    {
      sequelize,
      modelName: 'User',
      tableName: 'User',
      updatedAt: false,
    });
  return User;
};