
class DateWithTimezone extends Date {
    constructor() {
        super(Date.now() - (1000 * 60 * 60 * 3))
    }
}

module.exports = DateWithTimezone;