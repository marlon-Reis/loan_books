'use strict';

const { Op } = require("sequelize");
const models = require('../models')

class BookService {
    async create(title, page, ownerId) {
        return await models.Book.create({ title, page, ownerId })
    }

    findBookByTitleIgnoreCaseAndOwnerId(title, ownerId) {
        return models.Book.findOne({
            where: {
                [Op.and]: [{ title }, { ownerId }]
            }
        })
    }

    findBookByIdAndOwnerId(id, ownerId) {
        return models.Book.findOne({
            where: {
                [Op.and]: [{ id }, { ownerId }]
            }
        })
    }

    findBookById(id) {
        return models.Book.findByPk(id);
    }

}

module.exports = BookService;