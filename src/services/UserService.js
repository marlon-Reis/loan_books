'use strict';

const models = require('../models')

class UserService {

    async findUserById(id) {
        return await models.User.findByPk(id);
    }

    async findUserWithBookCollerctionById(id) {
        return await models.User.findOne({
            attributes: ['id', 'name', 'email'],
            include: [
                {
                    association: 'collections',
                    attributes: ['id', 'title', 'page']
                }],
            where: {
                id
            }

        })
    }

    static findUserByEmail(email) {
        return models.User.findOne({
            where: { email }
        });
    }

    async create(name, email) {
        return await models.User.create({ name, email })
    }

    async update(id, name, email) {
        const result = await models.User.update({ name, email }, { where: { id } })
        if (result[0]) {
            return await this.findUserById(id)
        }
        throw new Error("Register not updated")
    }

}

module.exports = UserService;