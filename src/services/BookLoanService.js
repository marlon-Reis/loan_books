'use strict';

const { Op } = require("sequelize");
const models = require('../models');
const DateWithTimezone = require("../util/DateWithTimezone");


const includeParames = [
    {
        association: 'borrowed_book',
        attributes: ['id', 'title', 'page']
    },
    {
        association: 'from_user',
        attributes: ['id', 'name', 'email']
    },
    {
        association: 'to_user',
        attributes: ['id', 'name', 'email']
    }
]

class BookLoanService {
    async lendBook(fromUser, toUser, book) {
        return await models.BookLoan.create({
            fromUser, toUser, book
        });
    }

    async findAllLendBook() {
        return await models.BookLoan.findAll({
            attributes: ['id', 'lentAt', 'returnedAt'],
            include: [...includeParames],
        });
    }

    thisBookIsOnLoan(book) {
        return models.BookLoan.findOne({
            where: {
                [Op.and]: [{ book }, { returnedAt: null }]
            }
        })
    }

    async findBookLoanById(id) {
        return await models.BookLoan.findOne({
            attributes: ['id', 'lentAt', 'returnedAt'],
            include: [...includeParames],
            where: {
                id
            }

        })
    }

    async findBookLoanByFromUserAndBookAndReturnedAt(fromUser, book, returnedAt) {
        return await models.BookLoan.findOne({
            attributes: ['id', 'lentAt', 'returnedAt'],
            include: [...includeParames],
            where: {
                [Op.and]: [{ fromUser }, { book }, { returnedAt }]
            }
        })
    }

    async returnTheBook(fromUser, book) {
        const returnedAt = new DateWithTimezone();
        const result = await models.BookLoan.update({
            fromUser, returnedAt
        },
            {
                where: {
                    [Op.and]: [{ book }, { returnedAt: null }]
                }
            }
        );

        if (result[0]) {
            return await this.findBookLoanByFromUserAndBookAndReturnedAt(fromUser, book, returnedAt)
        }

        throw new Error("Register not updated")
    }

}

module.exports = BookLoanService;