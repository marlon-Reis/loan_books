'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('User', [
      {
        name: 'John Doe',
        email: 'john@email.com.br',
        createdAt: new Date()
      }, {
        name: 'Antonio Carlos',
        email: 'antonio-carlos@email.com.br',
        createdAt: new Date()
      }, {
        name: 'Amanda Lorenzo',
        email: 'amanda-lorenzo@email.com.br',
        createdAt: new Date()
      }, {
        name: 'Fabio Braza',
        email: 'fabio-braza@email.com.br',
        createdAt: new Date()
      }, {
        name: 'Fernanda Mendes',
        email: 'fernanda-mendes@email.com.br',
        createdAt: new Date()
      }, {
        name: 'José Lucas',
        email: 'jose-lucas@email.com.br',
        createdAt: new Date()
      }
    ], {});

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User', null, {});
  }
};
