'use strict';

const router = require('./router')

const UserController = require('../controllers/UserController');
const userMiddleware = require('../middleware/user.middleware');
const checkValidator = require('../middleware/check-validator.middleware')

const controller = new UserController()

router.post('/users',
    userMiddleware.checkBodyAttribute,
    checkValidator.checkValidator,
    userMiddleware.emailIsUnique,
    controller.create
)

router.put('/users',
    userMiddleware.checkBodyAttribute,
    checkValidator.checkValidator,
    controller.update
)
router.get('/users/:id', controller.findUserById)
router.get('/users/:id/collerctions', controller.findUserWithBookCollerctionById)

module.exports = router;