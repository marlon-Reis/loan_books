'use strict';

const bodyParser = require('body-parser');

const userRouter = require('./user.router');
const bookRouter = require('./book.router');
const bookLoanRouter = require('./book-loan.router');

module.exports = app => {
    app.use(bodyParser.json())

    app.use('/loanbook', userRouter);
    app.use('/loanbook', bookRouter);
    app.use('/loanbook', bookLoanRouter);
}