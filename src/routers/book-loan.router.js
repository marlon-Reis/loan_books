
const router = require('./router')

const BookLoanController = require('../controllers/BookLoanController');
const validator = require('../middleware/check-validator.middleware');
const bookLoanMiddleware = require('../middleware/book-loan.middleware')

const controller = new BookLoanController()

router.post('/loans',
    bookLoanMiddleware.checkBodyAttribute,
    validator.checkValidator,
    bookLoanMiddleware.checksIfTheUserHasThisBook,
    bookLoanMiddleware.checkUsersAreDifferent,
    bookLoanMiddleware.checksIfThisBookIsOnLoan,
    controller.lendBook
);

router.put('/loans',
    bookLoanMiddleware.checkReturnTheBookBodyAttribute,
    validator.checkValidator,
    bookLoanMiddleware.checksIfCanReturnBook,
    controller.returnTheBook
);

router.get('/loans', controller.findAllLendBook);

router.get('/loans/:id', controller.findBookLoanById)

module.exports = router;