'use strict';

const router = require('./router')

const BookController = require('../controllers/BookController');
const bookMiddleware = require('../middleware/book.middleware');
const validator = require('../middleware/check-validator.middleware');

const controller = new BookController()

router.post('/books',
    bookMiddleware.checkBodyAttribute,
    validator.checkValidator,
    bookMiddleware.checksIfTheUserHasThisBook,
    bookMiddleware.checksIfUserExist,
    controller.create
)

module.exports = router;