'use strict';

const BookService = require('../services/BookService');
const service = new BookService();

class BookController {

    async create(req, res) {
        const { title, page, ownerId } = req.body;
        try {
            const result = await service.create(title, page, ownerId);
            return res.status(200).json(result);
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }
}

module.exports = BookController;