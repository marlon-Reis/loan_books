'use strict';

const BookLoanService = require('../services/BookLoanService');
const service = new BookLoanService();

class BookLoanController {

    async lendBook(req, res) {
        const { fromUser, toUser, book } = req.body;
        try {
            const result = await service.lendBook(fromUser, toUser, book);
            return res.status(200).json(result);
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    async findBookLoanById(req, res) {
        const { id } = req.params;
        try {
            const result = await service.findBookLoanById(id)
            if (result)
                return res.status(200).json(result);
            else
                return res.status(404).json({
                    msg: `Cannot found loan book by id='${id}'`
                });

        } catch (error) {
            return res.status(500).json(error.message)
        }
    }


    async findAllLendBook(req, res) {
        try {
            const result = await service.findAllLendBook()
            return res.status(200).json(result);
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }


    async returnTheBook(req, res) {
        const { fromUser, book } = req.body;
        try {
            const result = await service.returnTheBook(fromUser, book);
            return res.status(200).json(result);
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }
}

module.exports = BookLoanController;