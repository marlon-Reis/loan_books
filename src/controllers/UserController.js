'use strict';

const UserService = require('../services/UserService');
const service = new UserService();


class UserController {
    async findUserById(req, res) {
        const { id } = req.params;
        try {
            const result = await service.findUserById(id)
            if (result) {
                return res.status(200).json(result);
            }
            return res.status(404).json({
                msg: `Cannot found user by id='${id}'`
            });
        } catch (error) {
            console.log(JSON.stringify(error));

            return res.status(500).json(error.message)
        }
    }

    async findUserWithBookCollerctionById(req, res) {
        const { id } = req.params;
        try {
            const result = await service.findUserWithBookCollerctionById(id)
            if (result) {
                return res.status(200).json(result);
            }
            return res.status(404).json({
                msg: `Cannot found user by id='${id}'`
            });
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }


    async create(req, res) {
        const { name, email } = req.body;
        try {
            const result = await service.create(name, email)
            return res.status(200).json(result);
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    async update(req, res) {
        const { id, name, email } = req.body;
        try {
            const result = await service.update(id, name, email)
            return res.status(200).json(result);
        } catch (error) {
            return res.status(500).json({
                message: error.message
            })
        }
    }
}

module.exports = UserController;