# API de emprestimo de livro
Essa api possui o proposito de realizar o emprestimo de livros.

## Recursos utilizados 
- Docker
	- MySQL 5.7
- Node JS
	- Express Js
- Sequelize
- Bitbucket
	- Repositório 
	- Pipeline

# Primeiros passos

Para utilizar a api no ambiente de teste e desenvolvimento executo o comando a baixo para criar o conteiner docker.

Ambiente linux:


**sudo docker-compose up -d**


Ambiente window:

**docker-compose up -d**

### Executando a api


Para executar os teste:

**npm test**

Para executar a aplicação em modo de desenvolvimento: 
**npm run dev**

Produção:
**npm start**

**ATENÇÃO**
No arquivo *docker-compose.yml*  não possui a configuração do ambiente de produção, somente o ambiente de test e desenvolvimento. 

Para sebar mais sobre as configurações acesse o arquivo *src/config/config.json*

**Documentação Postman:**

[https://documenter.getpostman.com/view/1034132/T17Kcm8N](https://documenter.getpostman.com/view/1034132/T17Kcm8N "https://documenter.getpostman.com/view/1034132/T17Kcm8N")